**MetaSky - Gateway to the Metaverse**

<a href= "https://metasky.me/">Metasky</a> is the brand new gateway to Metaverse. It's an ecosystem that provides the foundation for a new class of digital asset. Metasky is a place to explore and customize your identity, interact with others in an inclusive and secure environment, and create objects which can be narratively embedded with your own experiences, knowledge, and ideas.
